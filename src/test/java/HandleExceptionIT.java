import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HandleExceptionIT {

    private static org.slf4j.Logger log = LoggerFactory.getLogger(HandleExceptionIT.class);

    @BeforeClass
    public static void setup() throws IOException {
        Path file = Paths.get("testdaten.json");

        Charset charset = Charset.forName("UTF-8");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            log.error("Error at setup time: " + e);
            throw(e);
        }
    }

    @Test
    public void Check() {
        log.info("Check()");
    }


}

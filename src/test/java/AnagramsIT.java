import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class AnagramsIT {

    private static Logger log = LoggerFactory.getLogger("AnagramsIT" /*"Anagrams"*/);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void areAnagramsITCase() {
        assertTrue(Anagrams.areAnagrams("amaryllis", "amaryllis"));
        log.info("@Test: areAnagrams()");

    }

    @Test
    public void areNotAnagramsITCase() {
        assertFalse(Anagrams.areAnagrams("amaryllis", "carnation"));
        log.info("@Test: areNotAnagrams()");
    }

    @Test
    public void findAnagramsITCase() {
        log.info("@Test: findAnagrams()");
        assertEquals("Strings differ...", "november", "february");
    }
}
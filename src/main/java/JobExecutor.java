import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;

public abstract class JobExecutor {
public enum Versions {LOCAL, DOCKER, OPENSHIFT}

    public abstract void connect();
    public abstract void connectToDatabase() throws java.io.IOException;
    public abstract void disconnectFromDatabase() throws java.io.IOException;

    public static JobExecutor instanceOf(Versions v) {
        switch (v) {
            case LOCAL:
                return new LocalJobExecutor();
            case DOCKER:
                break;
            case OPENSHIFT:
            break;
        }
        return new LocalJobExecutor();
    }

}

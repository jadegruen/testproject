
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Anagrams {
    private static Logger log = LoggerFactory.getLogger("Anagrams");

    protected Map<String, List<String>> wordMap = new HashMap<String, List<String>>();

    private String sortLetters(String w) {
        char[] charsW = w.toLowerCase().toCharArray();
        Arrays.sort(charsW);

        return new String(charsW);
    }


    public void addToWordList() {
        String alpha;

        ArrayList<String> words = new ArrayList<String>(32);
        words.add("d9g80f7");
        words.add("arrival");
        words.add("quaint");

        words.add("lavirrax");
        words.add("x4p3o2n1");
        words.add("lavirra");

        for (String word: words) {
            alpha = sortLetters(word);
            List <String> list; // = wordMap.get(alpha);
            wordMap.put(alpha, list = wordMap.get(alpha));
            if (list == null) {
                wordMap.put(alpha, list = new ArrayList<String>());
            }
            list.add(word);
        }


        System.out.println("\n\nIterating the Map with Map.Entry<String,String>...");
        for(Map.Entry<String, List<String>> entry: wordMap.entrySet()) {
            System.out.println(entry.getKey() + ": ");
            List<String> wordList = entry.getValue();

            System.out.print("\t");
            for (String w: wordList) {
                System.out.print(w + " ");
            }
            System.out.println();
        }
    }

    // arrest rarest
    // kinship pinkish
    // paste pates peats septa spate tapes tepas
    // punctilio unpolitic

    public static boolean areAnagrams(String w1, String w2) {
        if (w1 == null || w2 == null) return true;

        if (w1.length() != w2.length()) return false;

        char[] charsW1 = w1.toLowerCase().toCharArray();
        Arrays.sort(charsW1);

        char[] charsW2 = w2.toLowerCase().toCharArray();
        Arrays.sort(charsW2);

        String sortedW1 = new String(charsW1);
        String sortedW2 = new String(charsW2);

        return (sortedW1.equals(sortedW2));
    }

    public void findAnagrams(ArrayList<String> wordList) {

    }


}
